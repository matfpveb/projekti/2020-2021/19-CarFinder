const http = require('http');
const app = require('./app');

// Defining port
const port = process.env.PORT || 3000;

// Creating a server
const server = http.createServer(app);

// Initializing listener
server.listen(port, () => {
    console.log(`App is running on http://localhost:${port}`);
});
