import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomSearchFormComponent } from './custom-search-form.component';

describe('CustomSearchFormComponent', () => {
  let component: CustomSearchFormComponent;
  let fixture: ComponentFixture<CustomSearchFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomSearchFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
