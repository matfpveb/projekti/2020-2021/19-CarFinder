const { urlencoded, json } = require('body-parser');
const carService = require('../services/car');

const getManufacturers = async (req, res, next) => {
    try {
        const manufacturers = await carService.getManufacturers();

        return res.status(200).json({ manufacturers });
    } catch (err) {
        next(err);
    }
}

const getModels = async (req, res, next) => {
    try {
        const manufacturer = req.params.manufacturer;
        const models = await carService.getModels(manufacturer);

        return res.status(200).json({ models });
    } catch (err) {
        next(err);
    }
}

const getColors = async (req, res, next) => {
    try {
        const colors = await carService.getColors();

        return res.status(200).json({ colors });
    } catch (err) {
        next(err);
    }
}

const getCar = async (req, res, next) => {
    try {
        const { manufacturer, model, age } = req.body;
        const car = await carService.getCar(manufacturer, model, age);

        if (car.length == 0) {
            res.status(404).json();
        } else {
            res.status(200).json(car);
        }
    } catch (err) {
        next(err);
    }
}

const deleteCar = async (req, res, next) => {
    try {
        const { manufacturer, model, age } = req.body;
        const info = await carService.deleteCar(manufacturer, model, age);

        if (info.deletedCount == 1) {
            res.status(200).json();
        } else {
            res.status(404).json();
        }
    } catch (err) {
        next(err);
    }
}

const putCar = async (req, res, next) => {
    try {
        const { srcParams, updParams } = req.body;
        const info = carService.putCar(srcParams, updParams);

        if (info.ok == 0) {
            res.status(404).json();
        } else {
            res.status(200).json();
        }
    } catch (err) {
        next(err);
    }
}

const postCar = async (req, res, next) => {
    try {
        const info = await carService.postCar(req.body);

        res.status(200).json();
    } catch (err) {
        next(err);
    }
}

module.exports = {
    getManufacturers,
    getModels,
    getColors,
    getCar,
    deleteCar,
    putCar,
    postCar
}