const express = require('express');
const userController = require('../../controllers/user');
const auth = require('../../middleware/auth');

const router = express.Router();

// Registering or logging in
router.post('/signup', auth.checkDuplicateUsernameOrEmail, userController.signUp);
router.put('/signin', userController.signIn);

// Fetching users from the db
router.get('/', auth.verifyToken, auth.grantAccess("readAny", "profile"), userController.getUsers);
router.get('/:username', auth.verifyToken, auth.grantAccess("readOwn", "profile"), userController.getUser);

// Adding or deleting user from the db
router.put('/:username', auth.verifyToken, auth.grantAccess("updateOwn", "profile"), userController.putUser);
router.delete('/:username', auth.verifyToken, auth.grantAccess("deleteAny", "profile"), userController.deleteUser);

module.exports = router;