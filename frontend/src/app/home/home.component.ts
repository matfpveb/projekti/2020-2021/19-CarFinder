import { Component, OnInit } from '@angular/core';
import { SearchKind } from '../shared/search-kind';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  // we store in shared variable which search kind we have selected
  setSearchKind(sk: string): void {
    SearchKind.setSearchKind(sk);
  }

  ngOnInit(): void {
  }

}
