const mongoose = require('mongoose');
const { Ad } = require('../models/models');

const catDefault = [
    "manufacturer", 
    "model",
    "transmission", 
    "model_year",
    "engine",
    "color",
    "class"
];

const numDefault = [
    "price",
    "milleage",
    "car_year", 
    "displacement"
];

function refineAttrs(attrDefault, attrs, params) {
    attrDefault.forEach((attr, _) => {
        if (params[attr] === undefined) {
            attrs.splice(attrs.indexOf(attr), 1);
        }
    });
}

function createPopulateParams(params, catAttrs) {
    var populateParams = {}

    catAttrs.forEach((attr, _) => {
        populateParams[attr] = { $eq: params[attr] };
    })

    return populateParams;
}

function ranking(ads, params, numAttrs) {
    var ranksPerAttr = {}

    for (var i = 0; i < numAttrs.length; i++) {
        const attr = numAttrs[i];

        var argsort = ads
            .map((ad, i) => [ad[attr] - params[attr], i])
            .sort();

        var ranks = new Array(ads.length).fill(0);
        argsort.forEach((v, idx) => ranks[v[1]] = idx);
        ranksPerAttr[attr] = ranks;
    }

    var ranks = new Array(ads.length).fill(0);
    numAttrs.forEach((attr, _) => {
        ranks.forEach((_, idx) => {
            ranks[idx] += ranksPerAttr[attr][idx];
        });
    });

    ads.forEach((_, idx) => ads[idx].rank = ranks[idx])
    ads = ads.sort((a, b) => a.rank - b.rank);

    return ads;
}

const flattenObject = (obj) => {
    const flattened = {}
  
    Object.keys(obj).forEach((key) => {
      if (typeof obj[key] === 'object' && obj[key] !== null) {
        Object.assign(flattened, flattenObject(obj[key]))
      } else {
        flattened[key] = obj[key]
      }
    })
  
    return flattened
}

const adSearch = async(params, isSmart) => {
    var catAttrs = catDefault.slice();
    refineAttrs(catDefault, catAttrs, params);

    var numAttrs = numDefault.slice();
    refineAttrs(numDefault, numAttrs, params);

    const populateParams = createPopulateParams(params, catAttrs);

    var ads = await Ad
        .find({})
        .populate("car", null, populateParams)
        .populate("user", null, {})
        .exec()
    ads = ads.filter((ad, _) => ad.car != null);

    if (isSmart) {
        ads = ranking(ads, params, numAttrs);
    }

    return ads;
}

module.exports = adSearch;