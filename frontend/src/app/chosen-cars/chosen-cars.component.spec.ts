import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChosenCarsComponent } from './chosen-cars.component';

describe('ChosenCarsComponent', () => {
  let component: ChosenCarsComponent;
  let fixture: ComponentFixture<ChosenCarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChosenCarsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChosenCarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
