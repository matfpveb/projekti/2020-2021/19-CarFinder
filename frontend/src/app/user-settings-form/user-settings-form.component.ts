import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JWTService } from "../shared/jwt-service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-settings-form',
  templateUrl: './user-settings-form.component.html',
  styleUrls: ['./user-settings-form.component.css']
})
export class UserSettingsFormComponent implements OnInit {

  //url that we use as a constant in http request
  private readonly urls = {
    putUser: "http://localhost:3000/api/user"
  }

  //fields that need to be filled so we can change users information
  password: string = null;
  name: string = null;
  surname: string = null;
  city: string = null;
  phone: string = null;
  mail: string = null;
  age: number = null;

  constructor(private http: HttpClient) {}

  //if user done something wrong inform user about that
  public userSettingsInfo(text : string, buttonText : string){
    Swal.fire({
      //title: 'Warning',
      text: text,
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: buttonText,
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true
    })
  }

  // send http request that updates user information
  updateSettings(): void {
    var updParams = {
      "password": this.password,
      "name": this.name,
      "surname": this.surname,
      "city": this.city,
      "phone": this.phone,
      "email": this.mail,
      "age": this.age
    }

    //remove parameters that we didnt set
    Object.keys(updParams).forEach(key => updParams[key] == null && delete updParams[key])

    //get current user username
    let username = JWTService.getPayload().username
    //set access rights in request header
    const headers = { "x-access-token": JWTService.getAccessToken() };
    this.http.put<any>(this.urls.putUser + "/" + username, { updParams }, { headers }).subscribe({
      next: data => {
        //inform user about response we get from databse
        this.userSettingsInfo("The changes have been applied!", "OK")
      },
      error: error => {
        console.log(error);
        //inform user about response we get from databse
        this.userSettingsInfo("Wrong input!", "Try again")
      }
    });
  }

  ngOnInit(): void {
  }
}
