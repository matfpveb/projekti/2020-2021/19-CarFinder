const express = require('express');
const { urlencoded, json } = require('body-parser');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const path = require('path');

// Create web server by calling express() function
const app = express();

var cors = require('cors');
const { User } = require('./models/models');

require('dotenv').config();

// Connect to the database
const databaseString = "mongodb://localhost:27017/CarFinder";
mongoose.connect(databaseString, {
   useNewUrlParser: true,
   useUnifiedTopology: true,
   useFindAndModify: false
});

mongoose.connection.once('open', () => {
    console.log('Connection successful!');
});

mongoose.connection.on('error', error => {
    console.log('Error: ', error);
});

app.use(cors());

// Read request as json or key-value pair
app.use(json({ limit: 1024 * 1024 * 10 }));
app.use(urlencoded({ extended: false, limit: 1024 * 1024 * 10 }));

const userAPI = require("./routes/api/user")
app.use("/api/user", userAPI)

const carAPI = require("./routes/api/car")
app.use('/api/car', carAPI);

const adAPI = require('./routes/api/ad');
app.use('/api/ad', adAPI);

module.exports = app;