import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  //variables that sets visibility of selected tab
  signInShow: boolean = true;
  registerShow: boolean = false;
  constructor() { }

  //functions that switches context between selected form
  openSignIn() {
    this.registerShow = false;
    this.signInShow = true;
  }

  openRegister() {
    this.signInShow = false;
    this.registerShow = true;
  }

  ngOnInit(): void {
  }

}
