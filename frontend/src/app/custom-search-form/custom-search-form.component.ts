import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { JWTService } from "../shared/jwt-service";
import { DomSanitizer } from "@angular/platform-browser";
import { SearchKind } from "../shared/search-kind";
import Swal from 'sweetalert2';
import { Router } from "@angular/router";
import { MyAdsFormComponent } from "../my-ads-form/my-ads-form.component";

interface Choose_Option {
  name: string,
  code: string
};

@Component({
  selector: "app-custom-search-form",
  templateUrl: "./custom-search-form.component.html",
  styleUrls: ["./custom-search-form.component.css"]
})
export class CustomSearchFormComponent implements OnInit {

  //urls used as constants for http requests
  private readonly urls = {
    getManufacturers: "http://localhost:3000/api/car/manufacturers",
    getModels: "http://localhost:3000/api/car/",
    getAdComplete: "http://localhost:3000/api/ad/complete",
    getMyAds: "http://localhost:3000/api/ad/my",
    getColors: "http://localhost:3000/api/car/colors",
  }

  //limit of cars that we can store in favourite cars array
  private readonly CHOSEN_CARS_LIMIT: number = 5;

  //ads obtained via search
  ads: any[] = [];
  is_custom_search: boolean = false;

  // visibility of components that aren't always visible
  sidebar_visible: boolean = false;
  sidebar_sort_visible: boolean = false;
  show_settings: boolean = false;
  show_new_ad: boolean = false;
  show_my_ads: boolean = false;
  show_log_out: boolean = false;
  car_info_visibility: boolean = false;

  //sorting options
  sort_option: Choose_Option;
  possible_sorting_options: Choose_Option[];
  sort_method: Choose_Option;
  possible_sorting_methods: Choose_Option[];

  //car manufacturer
  car_manufacturer: Choose_Option = { name: "", code: "" };
  possible_manufacturers: Choose_Option[] = [];

  //car model
  car_model: Choose_Option = { name: "", code: "" };
  possible_models: Choose_Option[] = [];

  //car class
  car_class: Choose_Option = { name: "", code: "" };
  possible_classes: Choose_Option[];

  //car age
  model_year: number = null;

  //car price
  car_price: number = null;

  //car transmission
  car_transmission: Choose_Option = { name: "", code: "" };
  possible_transmissions: Choose_Option[];

  //car engine
  car_engine: Choose_Option = { name: "", code: "" };
  possible_engine: Choose_Option[];

  //car color
  car_color: Choose_Option = { name: "", code: "" };
  possible_colors: Choose_Option[] = [];

  //car drivetrain
  car_drivetrain: Choose_Option = { name: "", code: "" };
  possible_drivetrains: Choose_Option[];

  //car power
  car_displacement: Number = null;

  //chosen cars sidebar info
  favourite_cars_data = [];
  car_info_data: any = {};
  @ViewChild("chosenCarContainer", {
    static: false
  }) chosen_car_container;

  my_ads: any[] = [];
  @ViewChild(MyAdsFormComponent, {
    static: false
  }) my_ads_component;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer, private elEref: ElementRef,
    private router: Router) {
    this.possible_sorting_options = [
      { name: "Manufacturer", code: "manufacturer" },
      { name: "Model", code: "model" },
      { name: "Model year", code: "model_year" },
      { name: "Class", code: "class" },
      { name: "Transmission", code: "transmission" },
      { name: "Engine", code: "engine" },
      { name: "Car year", code: "car_year" },
      { name: "Color", code: "color" },
      { name: "Displacement", code: "displacement" },
      { name: "Price", code: "price" },
      { name: "Drivetrain", code: "drivetrain" },
      { name: "Mileage", code: "milleage" },
    ];

    // array of jsons that represents sorting order
    this.possible_sorting_methods = [
      { name: "Ascending", code: "Asc" },
      { name: "Descending", code: "Desc" }
    ];

    //initialize car transmissions as it is constant array
    this.possible_transmissions = [
      { name: "Continuously Variable Transmission (CVT)", code: "CVT" },
      { name: "Manual Transmission (MT)", code: "MT" },
      { name: "Automatic Transmission (AT)", code: "AT" },
      { name: "Automated Manual Transmission (AM)", code: "AM" },
    ];

    //initialize car engine as it is constant array
    this.possible_engine = [
      { name: "Diesel", code: "Diesel" },
      { name: "Petrol", code: "Petrol" },
    ];

    //initialize car classes as it is constant array
    this.possible_classes = [
      { name: "Cabriolet", code: "Cabriolet" },
      { name: "SUV", code: "SUV" },
      { name: "Wagon", code: "Wagon" },
      { name: "Sedan", code: "Sedan" },
      { name: "Hatchback", code: "Hatchback" }
    ];

    //initialize car drivetrains as it is constant array
    this.possible_drivetrains = [
      { name: "FWD", code: "FWD" },
      { name: "RWD", code: "RWD" },
      { name: "AWD", code: "AWD" },
      { name: "4WD", code: "4WD" }
    ]
  }

  // function that represents onInit load of possible car manufacturers
  load_manufacturers() {
    this.http.get<any>(this.urls.getManufacturers, {}).subscribe({
      next: data => {
        // fill our possible manufacturers array with data from our database
        for (let i in data["manufacturers"]) {
          this.possible_manufacturers.push({ name: data["manufacturers"][i], code: data["manufacturers"][i] });
        }

        // sort array in lexicographic order
        this.possible_manufacturers = this.possible_manufacturers.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      },
      error: error => {
        console.log(error);
      }
    })
  }

  //function that represents onInit load of possible colors from database
  load_colors() {
    this.http.get<any>(this.urls.getColors, {}).subscribe({
      next: data => {
        //fill array of possible colors
        for (let i in data["colors"]) {
          this.possible_colors.push({ name: data["colors"][i], code: data["colors"][i] });
        }

        //sort array
        this.possible_colors = this.possible_colors.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      },
      error: error => {
        console.log(error);
      }
    })
  }

  // once we select car manufacturer we load its possible models via following function
  load_models() {
    this.http.get<any>(this.urls.getModels + this.car_manufacturer.name, {}).subscribe({
      next: data => {

        //delete previous data in array
        this.possible_models.length = 0;

        //fill array with data we get from database
        for (let i in data["models"]) {
          this.possible_models.push({ name: data["models"][i], code: data["models"][i] });
        }

        //sort array
        this.possible_models = this.possible_models.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      },
      error: error => {
        console.log(error);
      }
    })
  }

  //function that sorts ads that we show
  sort_ads() {
    if (this.sort_option.code.length != 0 ||
      this.sort_method.code.length != 0 ||
      this.ads.length == 0) {
      return;
    }

    this.ads.sort((a, b) => {
      const aVal = a[this.sort_option.code];
      const bVal = b[this.sort_option.code];

      if (aVal == bVal) return 0;
      if (aVal > bVal) return 1;
      return -1;
    });

    if (this.sort_option.code.localeCompare("Desc") == 0) {
      this.ads = this.ads.reverse();
    }
  }

  //http request to get ads from database for selected filters
  load_ads() {
    let body = {}

    // fill filter properties
    if (this.car_manufacturer.name.trim().length != 0) {
      body["manufacturer"] = this.car_manufacturer.name;
    }
    if (this.car_model.name.trim().length != 0) {
      body["model"] = this.car_model.name;
    }
    if (this.car_class.name.trim().length != 0) {
      body["class"] = this.car_class.name;
    }
    if (this.car_transmission.name.trim().length != 0) {
      body["transmission"] = this.car_transmission.name;
    }
    if (this.car_engine.name.trim().length != 0) {
      body["engine"] = this.car_engine.name;
    }
    if (this.car_price != null) {
      body["price"] = this.car_price;
    }
    if (this.car_color.name.trim().length != 0) {
      body["color"] = this.car_color.name;
    }
    if (this.car_displacement != null) {
      body["displacement"] = this.car_displacement;
    }
    if (this.model_year != null) {
      body["model_year"] = this.model_year;
    }

    //set access token so we can access to database as we are signed in as a valid user
    const headers = { "x-access-token": JWTService.getAccessToken() };

    this.http
      .post<any>(this.urls.getAdComplete, body, { headers }).subscribe({
        next: data => {
          //here we catch response from database and store ads in local array
          this.ads = data;
          for (var i = 0; i < this.ads.length; i++) {
            this.ads[i].user = {
              "email": data[i].user.email,
              "phone": data[i].user.phone
            };
            const imgUrl = data[i].img
            this.ads[i].img = this.sanitizer.bypassSecurityTrustResourceUrl(imgUrl);
          }
        },
        error: error => {
          console.log(error);
        }
      });
  }

  //dynamically create tag divs where we show selected filters
  create_tag_element(tag_container, target_tag) {
    // create div that stores message
    let div = document.createElement("div");
    div.style.background = "rgba(82, 30, 99, 0.9)";
    div.style.textAlign = "center";
    div.style.color = "white";
    div.style.fontSize = "Large";
    div.style.cursor = "pointer";
    div.style.padding = "5px";
    div.style.marginLeft = "1vw";
    div.id = target_tag;

    //dynamically set event listeners for previously created divs
    div.addEventListener("click", (ev) => {
      this.remove_tag(target_tag);
    });
    div.addEventListener("mouseenter", (ev) => {
      div.style.transform = "scale(0.9, 0.9)";
    })

    div.addEventListener("mouseleave", (ev) => {
      div.style.transform = "scale(1.0, 1.0)";
    })

    //set div text to currently picked value
    if (target_tag == "manufacturer") {
      div.textContent = this.car_manufacturer.code;
    }
    if (target_tag == "model") {
      div.textContent = this.car_model.code;
    }
    if (target_tag == "class") {
      div.textContent = this.car_class.code;
    }
    if (target_tag == "model_year") {
      div.textContent = "Y: " + this.model_year.toString();
    }
    if (target_tag == "price") {
      div.textContent = "P: " + this.car_price.toString() + " $";
    }
    if (target_tag == "transmission") {
      div.textContent = this.car_transmission.code;
    }
    if (target_tag == "power") {
      div.textContent = "D: " + this.car_displacement.toString() + " HP";
    }
    if (target_tag == "engine") {
      div.textContent = this.car_engine.code;
    }
    if (target_tag == "color") {
      div.textContent = this.car_color.code;
    }
    if (target_tag == "drivetrain") {
      div.textContent = this.car_drivetrain.code;
    }

    //append new div on its place in DOM tree
    tag_container.appendChild(div);
  }

  set_tags(target_tag: string) {
    // get container to store tags
    let tag_container = document.getElementById("tag_container");
    let active_tag = document.getElementById("tag_container").querySelector("#" + target_tag);

    //if tag already exists just change its value
    if (active_tag != null) {
      if (active_tag.id == "manufacturer") {
        active_tag.textContent = this.car_manufacturer.code;
      }
      if (active_tag.id == "model") {
        active_tag.textContent = this.car_model.code;
      }
      if (active_tag.id == "class") {
        active_tag.textContent = this.car_class.code;
      }
      if (active_tag.id == "model_year") {
        active_tag.textContent = "Y: " + this.model_year.toString();
      }
      if (active_tag.id == "price") {
        active_tag.textContent = "P: " + this.car_price.toString() + " $";
      }
      if (active_tag.id == "transmission") {
        active_tag.textContent = this.car_transmission.code;
      }
      if (active_tag.id == "power") {
        active_tag.textContent = "D: " + this.car_displacement.toString() + " HP";
      }
      if (active_tag.id == "engine") {
        active_tag.textContent = this.car_engine.code;
      }
      if (active_tag.id == "color") {
        active_tag.textContent = this.car_color.code;
      }
      if (active_tag.id == "drivetrain") {
        active_tag.textContent = this.car_drivetrain.code;
      }

      return;
    }

    //call help function that creates new tag if we dont have div for specific tag already
    this.create_tag_element(tag_container, target_tag);
  }

  // popUp window handler function
  public popUpNotification(text : string){
    Swal.fire({
      text: text,
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: 'OK!',
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true
    })
  }

  // function that adds new car to chosen cars
  add_favourite(ad) {
    //there is limit for number of added cars
    if (this.favourite_cars_data.length == this.CHOSEN_CARS_LIMIT) {
      this.popUpNotification("Limit is reached!");
      return;
    }

    //checking for duplicates
    for (var i = 0; i < this.favourite_cars_data.length; i++) {
      if (this.favourite_cars_data[i].id == ad._id) {
        this.popUpNotification("This ad is already added!");
        return;
      }
    }

    //dynamically create new div that contains picture of car
    let div = document.createElement("div");
    div.id = "car_" + this.favourite_cars_data.length;
    div.style.width = "100%";
    div.style.marginTop = "5vh";
    div.style.cursor = "pointer";

    //add informations about car that we like into array
    let data = {
      id: ad._id,
      manufacturer: ad.car.manufacturer, model: ad.car.model, model_year: ad.car.model_year,
      drivetrain: ad.car.drivetrain, price: ad.price, bargaining: ad.bargaining, registration: ad.registration,
      mileage: ad.milleage, color: ad.car.color, transmission: ad.car.transmission, description: ad.description,
      phone: ad.user.phone, email: ad.user.email, class: ad.car.class
    };

    this.favourite_cars_data.push(data);

    //create picture element and add it to the previously created tag
    let picture = document.createElement("img");
    picture.src = ad.img.changingThisBreaksApplicationSecurity;
    picture.alt = "Picture of favourite car";
    picture.style.width = "100%";

    //add event listener to the picture so we can show information about created car
    picture.addEventListener("click", (ev) => {
      this.show_car_info(div.id);
    });

    //append picture to div and div to DOM tree
    div.appendChild(picture);
    this.chosen_car_container.nativeElement.append(div);
  }

  // function that removes tags from filter bar
  remove_tag(target_tag: string) {
    let close_tag = document.getElementById("tag_container").querySelector("#" + target_tag);
    if (close_tag != null) {
      close_tag.remove();
    }
  }

  //function that shows popUp window which contains all information chosen about car
  show_car_info(index: string) {
    this.car_info_visibility = true;
    let car_index = index.charAt(index.length - 1)

    this.car_info_data = this.favourite_cars_data[car_index];
  }

  load_my_ads() {
    let body = { "username": JWTService.getPayload().username };
    const headers = { "x-access-token": JWTService.getAccessToken() };

    this.http
      .post<any>(this.urls.getMyAds, body, { headers }).subscribe({
        next: data => {
          //stores values from database into local array
          this.my_ads = data;
          for (var i = 0; i < this.my_ads.length; i++) {
            this.my_ads[i].user = {
              "email": data[i].user.email,
              "phone": data[i].user.phone
            };
            const imgUrl = data[i].img
            this.my_ads[i].img = this.sanitizer.bypassSecurityTrustResourceUrl(imgUrl);
          }
        },
        error: error => {
          console.log(error);
        }
      });
  }

  //function that happens each time we initialize page component
  ngOnInit(): void {
    if (JWTService.getPayload() == null) {
      this.router.navigateByUrl("/login");
    } else {
      this.is_custom_search = SearchKind.getSearchKind() == "custom";
      this.load_manufacturers();
      this.load_colors();
    }
  }
}
