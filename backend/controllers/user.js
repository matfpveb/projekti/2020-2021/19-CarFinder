const { User } = require('../models/models.js');
const userServices = require('../services/user');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const signUp = async (req, res, next) => {
    try {
        if (!("password" in req.body) || !("username" in req.body)) {
            return res.status(403).json({ error: "Username and password are mandatory"});
        }

        const body = req.body;
        body.password = bcrypt.hashSync(body.password, 10);
        body.role = body.role || "basic";

        const newUser = new User(body);
        newUser._id = mongoose.Types.ObjectId();

        const accessToken = jwt.sign({ userId: newUser._id, role: body.role },
            process.env.JWT_SECRET, {
            expiresIn: "30d"
        });

        newUser.accessToken = accessToken;
        try {
            await newUser.save();
        } catch (err) {
            return res.status(403).json({ error: "Invalid profile format"});
        }
        res.status(200).json({
            payload: { username: newUser.username, role: newUser.role },
            accessToken
        });
    } catch (err) {
        next(err);
    }
}

const signIn = async (req, res, next) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(404).json({ error: "User not found" });
        }

        const isValidPassword = bcrypt.compareSync(password, user.password);
        if (!isValidPassword) {
            return res.status(401).json({ error: "Invalid password" });
        }

        const accessToken = jwt.sign({ userId: user._id, role: user.role },
            process.env.JWT_SECRET, {
            expiresIn: "30d"
        });

        await User.findByIdAndUpdate(user._id, { accessToken });
        res.status(200).json({
            payload: { username: user.username, role: user.role },
            accessToken
        });
    } catch (err) {
        next(err);
    }
}

const getUser = async (req, res, next) => {
    try {
        const user = await userServices.getUser(req.params.username);
        if (!user) {
            res.status(404).json();
        } else {
            res.status(200).json({ data: user });
        }
    } catch (err) {
        next(err);
    }
}

const getUsers = async (req, res, next) => {
    try {
        const users = await userServices.getUsers();
        res.status(200).json(users);
    } catch(err) {
        next(err);
    }
}

const putUser = async(req, res, next) => {
    try {
        const username = req.params.username;
        const { updParams } = req.body;
        if ("password" in updParams) {
            updParams["password"] = bcrypt.hashSync(updParams["password"], 10);
        }

        const info = await userServices.putUser(username, updParams);

        if (info.ok == 0) {
            res.status(404).json();
        } else {
            res.status(200).json();
        }
    } catch (err) {
        next(err);
    }
}

const deleteUser = async (req, res, next) => {
    try {
        const username = req.params.username;
        const info = await userServices.deleteUser(username);

        if (info.deletedCount == 1) {
            res.status(200).json();
        } else {
            res.status(404).json();
        }
    } catch (err) {
        next(err);
    }
}

module.exports = {
    signUp,
    signIn,
    getUser,
    getUsers,
    putUser,
    deleteUser
}