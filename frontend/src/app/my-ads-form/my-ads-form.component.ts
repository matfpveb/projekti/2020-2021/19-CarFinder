import { Component, Input, OnInit } from '@angular/core';
import { JWTService } from '../shared/jwt-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import Swal from "sweetalert2"

@Component({
  selector: 'app-my-ads-form',
  templateUrl: './my-ads-form.component.html',
  styleUrls: ['./my-ads-form.component.css']
})
export class MyAdsFormComponent implements OnInit {

  private readonly urls = {
    deleteAd: "http://localhost:3000/api/ad/"
  }

  @Input() my_ads: any[];

  public deletedAdAlert() {
    Swal.fire({
      //title: 'Warning',
      text: 'Ad is already deleted!',
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: 'Try again!',
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true,
    })
  }

  public deletedAd() {
    Swal.fire({
      //title: 'Warning',
      text: 'Ad has been deleted',
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: 'OK',
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true,
    })
  }

  delete_ad(ad) {
    const options = {
      headers: new HttpHeaders({
        "x-access-token": JWTService.getAccessToken()
      }),
      body: {
        "_id": ad._id
      }
    };

    this.http
      .delete<any>(this.urls.deleteAd, options).subscribe({
        next: data => {
          this.deletedAd();
        },
        error: error => {
          this.deletedAdAlert();
        }
      });
  }

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  ngOnInit() {}
}
