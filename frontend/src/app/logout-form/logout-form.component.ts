import { Component, OnInit } from '@angular/core';
import { JWTService } from "../shared/jwt-service";
import {CustomSearchFormComponent} from '../custom-search-form/custom-search-form.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-logout-form',
  templateUrl: './logout-form.component.html',
  styleUrls: ['./logout-form.component.css']
})
export class LogoutFormComponent implements OnInit {

  constructor(public outerClass : CustomSearchFormComponent, private router: Router) { }

  //if we decide to logout, we remove access of user to the database and send user to login page
  logout() {
    JWTService.removeAccessToken();
    this.router.navigateByUrl("/login");
  }

  //after we make our decision we collapse logout window
  collapse() {
    this.outerClass.show_log_out = false;
  }

  ngOnInit(): void {
  }

}
