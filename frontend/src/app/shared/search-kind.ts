import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class SearchKind {
    private static ID_SEARCH_KIND: string = "SEARCH_KIND";

    constructor() { }

    public static getSearchKind(): string | any {
        const searchKind: string | any = localStorage.getItem(this.ID_SEARCH_KIND);

        if (!searchKind) {
            return null;
        }
        return searchKind;
    }

    public static setSearchKind(searchKind: string): void {
        localStorage.setItem(this.ID_SEARCH_KIND, searchKind);
    }
}