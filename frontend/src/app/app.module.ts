import { HomeComponent } from './home/home.component';
import { NgModule, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { SliderModule } from 'primeng/slider';
import { CarouselModule } from 'primeng/carousel';
import { SidebarModule } from 'primeng/sidebar';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SlideshowBackgroundComponent } from './login/slideshow-background.component';



import { LoginFormComponent } from './login-form/login-form.component';
import { SignInComponent } from './sign-in-form/sign-in.component';
import { RegisterComponent } from './register-form/register.component';
import { FormsModule } from '@angular/forms';
import { CustomSearchComponent } from './custom-search/custom-search.component';
import { CustomSearchFormComponent } from './custom-search-form/custom-search-form.component';
import { MenubarComponent } from './menubar/menubar.component';
import { CarouselBoxComponent } from './carousel-box/carousel-box.component';
import { ChosenCarsComponent } from './chosen-cars/chosen-cars.component';
import { SmartSearchComponent } from './smart-search/smart-search.component';
import { UserSettingsFormComponent } from './user-settings-form/user-settings-form.component';
import { TooltipModule } from 'primeng/tooltip';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import { MyAdsFormComponent } from './my-ads-form/my-ads-form.component';
import { LogoutFormComponent } from './logout-form/logout-form.component';
import { NewAdFormComponent } from './new-ad-form/new-ad-form.component';

const routes: Routes = [

  { path: 'login', component: SlideshowBackgroundComponent },
  { path: 'home', component: HomeComponent },
  { path: 'custom-search', component: CustomSearchComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'smart-search', component: SmartSearchComponent },
  { path: '', redirectTo: '/slideshow-background', pathMatch: 'full' },
]

@NgModule({
  declarations: [
    AppComponent,
    SlideshowBackgroundComponent,
    LoginFormComponent,
    SignInComponent,
    RegisterComponent,
    HomeComponent,
    CustomSearchComponent,
    CustomSearchFormComponent,
    MenubarComponent,
    CarouselBoxComponent,
    ChosenCarsComponent,
    SmartSearchComponent,
    UserSettingsFormComponent,
    MyAdsFormComponent,
    LogoutFormComponent,
    NewAdFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatSlideToggleModule,
    ButtonModule,
    SliderModule,
    InputTextModule,
    FormsModule,
    CarouselModule,
    SidebarModule,
    DropdownModule,
    DialogModule,
    TooltipModule,
    InputTextareaModule,
    ScrollPanelModule
  ],
  providers: [CustomSearchFormComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
