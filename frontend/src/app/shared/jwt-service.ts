import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class JWTService {
    private static ID_ACCESS_TOKEN: string = "JWT_ACCESS_TOKEN";
    private static ID_PAYLOAD: string = "PAYLOAD";

    constructor() { }

    public static getAccessToken(): string | null {
        const accessToken: string | null = localStorage.getItem(this.ID_ACCESS_TOKEN);

        if (!accessToken) {
            return null;
        }
        return accessToken;
    }

    public static getPayload(): any | null {
        const payload: any | null = localStorage.getItem(this.ID_PAYLOAD);
        if (!payload) {
            return null;
        }

        return JSON.parse(payload);
    }

    public static setAccessTokenPayload(accessToken: string, payload: any): void {
        localStorage.setItem(this.ID_ACCESS_TOKEN, accessToken);
        localStorage.setItem(this.ID_PAYLOAD, JSON.stringify(payload));
    }

    public static removeAccessToken(): void {
        localStorage.removeItem(this.ID_ACCESS_TOKEN);
        localStorage.removeItem(this.ID_PAYLOAD);
    }
}