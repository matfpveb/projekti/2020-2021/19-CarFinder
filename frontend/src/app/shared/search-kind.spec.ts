import { TestBed } from '@angular/core/testing';

import { SearchKind } from './search-kind';

describe('SearchKind', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchKind = TestBed.get(SearchKind);
    expect(service).toBeTruthy();
  });
});
