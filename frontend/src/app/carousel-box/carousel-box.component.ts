import { Component, OnInit } from '@angular/core';
import { tsCreateElement } from '@angular/compiler-cli/src/ngtsc/typecheck/src/ts_util';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { error } from 'selenium-webdriver';
import { JWTService } from "../shared/jwt-service";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser"


interface Car_Class {
  name: string,
  code: string
}

interface Car_Manufacturers {
  name: string,
  code: string
}

interface Car_Models {
  name: string,
  code: string
}

interface Car_Transmissions {
  name: string,
  code: string
}

interface Car_Engines {
  name: string,
  code: string
}

interface Car_Power {
  name: string,
  code: string
}

interface Sort_Options {
  option: string,
  code: string
}

interface Sort_Methods {
  method: string,
  code: string
}
@Component({
  selector: 'app-carousel-box',
  templateUrl: './carousel-box.component.html',
  styleUrls: ['./carousel-box.component.css']
})
export class CarouselBoxComponent implements OnInit {

  newDivs: addDivisions[] = [];

  addNewDiv() {
   this.newDivs.push(new addDivisions())
  }

  removeDiv(){
    this.newDivs.pop();
  }

  images : string[] = ["../../assets/Images/honda.jpg","../../assets/Images/nissan.jpg","../../assets/Images/wv.jpg"];

  i: number = 0;
  j: number = 0;

  nextImage(){
    if (this.i < this.images.length-1)
      this.i=this.i+1;
    if (this.j < this.images.length)
      this.j=this.j+1;
  }
  private readonly urls = {
    getManufacturers: "http://localhost:3000/api/car/manufacturers",
    getModels: "http://localhost:3000/api/car/",
    getAdComplete: "http://localhost:3000/api/ad/complete"
  }

  // visibility
  sidebar_visible: boolean = false;
  sidebar_cars_visible: boolean = false;
  show_settings: boolean = false;
  show_new_ad: boolean = false;
  show_my_ads: boolean = false;
  show_car_info: boolean[] = [false, false, false];

  //sorting options
  sort_option: Sort_Options;
  possible_sorting_options: Sort_Options[];
  sort_method: Sort_Methods;
  possible_sorting_methods: Sort_Methods[];


  //car manufacturer
  car_manufacturer: Car_Manufacturers = { name: "", code: "" };
  possible_manufacturers: Car_Manufacturers[] = [];

  //car model
  car_model: Car_Models = { name: "", code: "" };
  possible_models: Car_Models[] = [];

  //car class
  selectedClass: Car_Class = { name: "", code: "" };
  car_class: Car_Class[];

  //car age
  model_year: number = 1979;

  //car price
  car_price_range: number[] = [0, 0];

  //car transmission
  car_transmission: Car_Transmissions = { name: "", code: "" };
  possible_transmissions: Car_Transmissions[];

  //car engine
  car_engine: Car_Engines = { name: "", code: "" };
  possible_engine: Car_Engines[];

  //car color
  car_color: string;
  colors: string[];

  //car power
  car_power: Car_Power = { name: "", code: "" };
  possible_power: Car_Power[];

  items: any[] = [];



  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {


    this.possible_sorting_methods = [
      { method: 'Ascending', code: 'BMW' },
      { method: 'Descending', code: 'SUV' }
    ];



    //initialize car transmissions
    this.possible_transmissions = [
      { name: "Continuously Variable Transmission (CVT)", code: 'CVT' },
      { name: "Manual Transmission (MT)", code: 'MT' },
      { name: "Automatic Transmission (AT)", code: 'AT' },
      { name: "Automated Manual Transmission (AM)", code: 'AM' },
    ];

    //initialize car engine
    this.possible_engine = [
      { name: 'Engine1', code: 'BMW' },
      { name: 'Engine2', code: 'SUV' },
      { name: 'Engine3', code: 'WAG' },
    ];

    //initialize car power
    this.possible_power = [
      { name: 'Power1', code: 'BMW' },
      { name: 'Power2', code: 'SUV' },
      { name: 'Power3', code: 'WAG' },
    ];

    //initialize car classes
    this.car_class = [
      { name: 'Cabriolet', code: 'CAB' },
      { name: 'Suv', code: 'SUV' },
      { name: 'Wagon', code: 'WAG' },
      { name: 'Sedan', code: 'SED' },
      { name: 'Hatchback', code: 'HAT' }
    ];

    //initialize possible colors
    this.colors = ['Black', 'White', 'Blue', 'Yellow', 'Green', 'Gray', 'Orange', 'Red'];

  }

  load_manufacturers() {
    this.http.get<any>(this.urls.getManufacturers, {}).subscribe({
      next: data => {
        for (let i in data['manufacturers']) {
          this.possible_manufacturers.push({ name: data['manufacturers'][i], code: data['manufacturers'][i] });
        }

        this.possible_manufacturers = this.possible_manufacturers.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))


      },
      error: error => {
        console.log(error);
      }
    })
  }
  load_models() {
    this.http.get<any>(this.urls.getModels + this.car_manufacturer.name, {}).subscribe({
      next: data => {

        this.possible_models.length = 0;

        for (let i in data['models']) {
          this.possible_models.push({ name: data['models'][i], code: data['models'][i] });
        }

        this.possible_models = this.possible_models.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))


      },
      error: error => {
        console.log(error);
      }
    })
  }

  load_ad() {

    let body = {}

    if (this.car_manufacturer.name != "") {
      body['manufacturer'] = this.car_manufacturer.name;
    }
    if (this.car_model.name != "") {
      body['model'] = this.car_model.name;
    }
    if (this.selectedClass.name != "") {
      body['class'] = this.selectedClass.name;
    }
    if (this.car_transmission.name != "") {
      body['transmission'] = this.car_transmission.name;
    }
    if (this.car_engine.name != "") {
      body['engine'] = this.selectedClass.name;
    }
    if (this.car_price_range[1] != 0) {
      body['price'] = this.car_price_range[1];
    }

    const headers = { "x-access-token": JWTService.getAccessToken() };

    this.http
      .post<any>(this.urls.getAdComplete, body, { headers }).subscribe({
        next: data => {
          this.items = data;
        },
        error: error => {
          console.log(error);
        }
      });
  }

  ngOnInit(): void {
    this.load_manufacturers();
  }

  pick_color(color: string) {
    this.car_color = color;
    document.getElementById(color).style.borderWidth = "5px";

    for (let c in this.colors) {
      if (this.colors[c] != color) {
        document.getElementById(this.colors[c]).style.borderWidth = "2px";
      }
    }

    this.set_tags('color');
  }

  set_tags(target_tag: string) {
    // get container to store tags
    let tag_container = document.getElementById('tag_container');
    let active_tag = document.getElementById('tag_container').querySelector('#' + target_tag);

    //if tag already exists just change its value
    if (active_tag != null) {
      if (active_tag.id == 'manufacturer') {
        active_tag.textContent = this.car_manufacturer.name;
      }
      if (active_tag.id == 'model') {
        active_tag.textContent = this.car_model.name;
      }
      if (active_tag.id == 'class') {
        active_tag.textContent = this.selectedClass.name;
      }
      if (active_tag.id == 'age') {
        active_tag.textContent = this.model_year.toString();
      }
      if (active_tag.id == 'price') {
        active_tag.textContent = (this.car_price_range[0].toString() + "-" +
          this.car_price_range[1].toString());
      }
      if (active_tag.id == 'transmission') {
        active_tag.textContent = this.car_transmission.name;
      }
      if (active_tag.id == 'power') {
        active_tag.textContent = this.car_power.name;
      }
      if (active_tag.id == 'engine') {
        active_tag.textContent = this.car_engine.name;
      }
      if (active_tag.id == 'color') {
        active_tag.textContent = this.car_color[0].toUpperCase() + this.car_color.substr(1);
      }
      return;
    }


    // create div that stores message
    let div = document.createElement("div");
    div.style.background = 'rgba(82, 30, 99, 0.9)';
    div.style.height = '3vh'
    div.style.marginLeft = '1vw';
    div.style.color = 'white';
    div.style.fontSize = 'Large';
    div.id = target_tag;

    div.addEventListener('click', (ev) => {
      this.removeTag(target_tag);
    });
    div.addEventListener('mouseenter', (ev) => {
      div.style.background = 'rgba(194, 8, 26, 0.9)';
    })

    div.addEventListener('mouseleave', (ev) => {
      div.style.background = 'rgba(82, 30, 99, 0.9)';
    })


    if (target_tag == 'manufacturer') {
      div.textContent = this.car_manufacturer.name;
    }
    if (target_tag == 'model') {
      div.textContent = this.car_model.name;
    }
    if (target_tag == 'class') {
      div.textContent = this.selectedClass.name;
    }
    if (target_tag == 'age') {
      div.textContent = this.model_year.toString();
    }
    if (target_tag == 'price') {
      div.textContent = (this.car_price_range[0].toString() + "-" +
        this.car_price_range[1].toString());
    }
    if (target_tag == 'transmission') {
      div.textContent = this.car_transmission.name;
    }
    if (target_tag == 'power') {
      div.textContent = this.car_power.name;
    }
    if (target_tag == 'engine') {
      div.textContent = this.car_engine.name;
    }
    if (target_tag == 'color') {
      div.textContent = this.car_color[0].toUpperCase() + this.car_color.substr(1);
    }

    tag_container.appendChild(div);

  }

  removeTag(target_tag: string) {
    let close_tag = document.getElementById('tag_container').querySelector('#' + target_tag);
    if (close_tag != null) {

      if (target_tag == 'manufacturer') {
        this.car_manufacturer.name = "";
      }
      if (target_tag == 'model') {
        this.car_model.name = "";
      }
      if (target_tag == 'class') {
        this.selectedClass.name = "";
      }
      if (target_tag == 'age') {
        this.model_year = 1979;
      }
      if (target_tag == 'price') {
        this.car_price_range[0] = 0;
      }
      if (target_tag == 'transmission') {
        this.car_transmission.name = "";
      }
      if (target_tag == 'power') {
        this.car_power.name = "";
      }
      if (target_tag == 'engine') {
        this.car_engine.name = "";
      }
      if (target_tag == 'color') {
        this.car_color = "";
      }


      close_tag.remove();
    }

  }

  print_price() {
    let label = document.getElementById("price-label");
    label.textContent = "Price: " + this.car_price_range[0].toString() + "-" + this.car_price_range[1].toString();
  }

}

export class addDivisions { }
