import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { JWTService } from '../shared/jwt-service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  //url that we use as constant in http request
  private readonly urls = {
    postUser: "http://localhost:3000/api/user/signup"
  }

  constructor(private http: HttpClient, private router: Router) { }

  //initialize all fields on null so user must fill them
  username: string = null;
  password: string = null;
  name: string = null;
  surname: string = null;
  city: string = null;
  phone: string = null;
  mail: string = null;
  age: number = null;

  //popUp window that shows if user left some filed unfilled
  public incompleteRegistration(){
    Swal.fire({
      //title: 'Warning',
      text: 'You need to fill all fields',
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: 'OK!',
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true,
    })
  }

  // http request that signs up user
  public signUp(): void {
    var body = {
      "username": this.username,
      "password": this.password,
      "name": this.name,
      "surname": this.surname,
      "city": this.city,
      "phone": this.phone,
      "email": this.mail,
      "age": this.age
    }

    Object.keys(body).forEach(key => body[key] == null && delete body[key])

    this.http.post<any>(this.urls.postUser, body).subscribe({
      next: data => {
        //if we done everything correctly we can pass to the next page by getting access token
        JWTService.setAccessTokenPayload(data.accessToken, data.payload);
        this.router.navigateByUrl("/custom-search")
      },
      error: error => {
        //if we didnt fill all fields we inform user about that
        console.log(error);
        this.incompleteRegistration();
      }
    });
  }

  ngOnInit(): void {
  }
}
