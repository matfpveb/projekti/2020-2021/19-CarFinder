const AccessControl = require("accesscontrol");
const ac = new AccessControl();

// Defining roles and their access rights
exports.roles = (() => {
    ac.grant("basic")
        .readOwn("profile")
        .updateOwn("profile")
        .deleteOwn("profile")
        .createOwn("ad")
        .readAny("ad")
        .deleteOwn("ad")
    
    ac.grant("admin")
        .extend("basic")
        .updateAny("profile")
        .deleteAny("profile")
        .deleteAny("ad")
        .readAny("car")
        .createAny("car")
        .updateAny("car")
        .deleteAny("car")
    
    return ac;
})();