const mongoose = require('mongoose');
const { Car } = require('../models/models');

const getManufacturers = async() => {
    const cars = await Car.find({}).exec();

    var manufacturers = cars.map(car => car.manufacturer);
    manufacturers = [...new Set(manufacturers)];

    return manufacturers;
}

const getModels = async(manufacturer) => {
    const cars = await Car.find({ manufacturer}).exec();

    var models = cars.map(car => car.model);
    models = [...new Set(models)];

    return models;
}

const getColors = async() => {
    const cars = await Car.find({}).exec();

    var colors = cars.map(car => car.color);
    colors = [...new Set(colors)];

    return colors;
}

const getCar = async(manufacturer, model, age) => {
    const car = await Car.find({ manufacturer, model, age }).exec();
    return car;
}

const putCar = async(srcParams, updParams) => {
    Object.keys(srcParams).forEach(key => srcParams[key] === undefined && delete srcParams[key]);
    Object.keys(updParams).forEach(key => updParams[key] === undefined && delete updParams[key]);

    return Car.updateOne(srcParams, { $set: updParams });
}

const postCar = async(body) => {
    return Car.insertMany([body]);
}

const deleteCar = async(manufacturer, model, age) => {
    const info = await Car.deleteOne({ manufacturer, model, age }).exec();
    return info;
}

module.exports = {
    getManufacturers,
    getModels,
    getColors,
    getCar,
    deleteCar,
    putCar,
    postCar
}