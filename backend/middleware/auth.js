const jwt = require('jsonwebtoken')
const { roles } = require('./roles');
const { User } = require('../models/models');

const checkDuplicateUsernameOrEmail = (req, res, next) => {
    User.findOne({
        email: req.body.email
    }).exec((err, user) => {
        if (err) {
            return res.status(500).json({ error: "Internal error"});
        }

        if (user) {
            return res.status(400).json({ error: "Email already taken!"});
        }

        User.findOne({
            username: req.body.username
        }).exec((err, user) => {
            if (err) {
                return res.status(500).json({ error: "Internal error" });
            }

            if (user) {
                return res.status(400).json({ error: "Username already taken!" });
            }

            next();
        });
    });
}

const verifyToken = (req, res, next) => {
    const accessToken = req.headers["x-access-token"];
    if (!accessToken) {
        return res.status(403).json({ error: "No token provided" });
    }

    jwt.verify(accessToken, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            return res.status(401).json({ error: "Unauthorized!" });
        }

        req.userId = decoded.userId;
        req.role = decoded.role;
        next();
    })
}

const grantAccess = (action, resource) => {
    return async (req, res, next) => {
        try {
            const permission = roles.can(req.role)[action](resource);
            if (!permission.granted) {
                return res.status(401).json({ error: "You don't have proper permissions" });
            }

            next();
        } catch (err) {
            next(err);
        }
    }
}

module.exports = {
    checkDuplicateUsernameOrEmail,
    verifyToken,
    grantAccess
}