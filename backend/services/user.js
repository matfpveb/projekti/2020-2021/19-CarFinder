const mongoose = require('mongoose');
const { User } = require('../models/models');

const getUser = async (username) => {
    const user = await User.findOne({ username });
    return user;
}

const getUsers = async () => {
    const users = await User.find({}).exec();
    return users;
}

const putUser = async (username, updParams) => {
    return User.updateOne({ username }, { $set: updParams });
}

const deleteUser = async (username) => {
    const info = await User.deleteOne({ username }).exec();
    return info;
}

module.exports = {
    getUser,
    putUser,
    getUsers,
    deleteUser
}