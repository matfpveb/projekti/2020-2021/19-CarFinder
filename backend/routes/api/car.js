const express = require('express');
const carController = require('../../controllers/car');
const auth = require('../../middleware/auth');

const router = express.Router();

// Fetching all manufacturers and colors from db
router.get('/manufacturers', carController.getManufacturers);
router.get('/colors', carController.getColors);

// Fetching all existing models for a given manufacturer
router.get('/:manufacturer', carController.getModels);

// Fetching car from database by manufacturer, model and age
router.get('/', auth.verifyToken, auth.grantAccess("readAny", "car"), carController.getCar);

// Adding, updating or deleting car from db 
router.put('/', auth.verifyToken, auth.grantAccess("updateAny", "car"), carController.putCar);
router.post('/', auth.verifyToken, auth.grantAccess("createAny", "car"), carController.postCar);
router.delete('/', auth.verifyToken, auth.grantAccess("deleteAny", "car"), carController.deleteCar);

module.exports = router;