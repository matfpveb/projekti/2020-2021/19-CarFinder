# Project CarFinder 

Sajt koji nudi mogucnost prodaje i kupovine automobila i njihovih delova sporom i brzom pretragom.Brzom pretragom, na osnovu odabranih stavki,dobijamo automobil koji najbolje odgovara kriterijumima,dok se kod spore pretrage izlistavaju svi automobili koji odgovaraju kriterijumima.Korisnik moze postaviti novi oglas i izlistati svoje postavljene oglase.Oglasi se mogu filtrirati,sortirati i odabrani automobili se mogu pamtiti.

## Zavisnosti
Od dodatnih paketa/alata je potrebno imati instaliran mongodb i Angular.

## Pokretanje
1. pokrenuti `npm install` u direktorijumu `./frontend` (zavisnosti klijenta)
2. pokrenuti `npm install` u direktorijumu `./backend` (zavisnosti servera)
3. pokrenuti `node createJoinedJSON.js` u direktorijumu `./backend` (pravljenje baze podataka)
4. pokrenuti `node initDB.js` u direktorijumu `./backend` (inicijalizacija baze podataka)
5. pokrenuti `nodemon server.js` u direktorijumu `./backend` (pokretanje servera)
6. pokrenuti `ng serve` u direktorijumu `./frontend` (pokretanje klijenta)

## Baza podataka
[Šema baze podataka.](https://docs.google.com/spreadsheets/d/1uFyJz98E5A-8uz-BpGgKuibEBV0-TtZEzGMcq_Ma-R0/edit?usp=sharing)

## Prateći video materijal
[Snimak.](https://drive.google.com/file/d/11Vqb6Xi87e5cbJ948YzZ0JuBgg26smUQ/view)

## Developers

- [Momcilo Knezevic, 189/2017](https://gitlab.com/momciloknezevic7)
- [Jovan Rumenic, 69/2017](https://gitlab.com/rumeni1)
- [David Nestorovic, 162/2017](https://gitlab.com/dnestorovic)
- [Kosta Grujcic, 12/2017](https://gitlab.com/4eyes4u)
- [Filip Bozovic, 43/2017](https://gitlab.com/Smrznuti)
