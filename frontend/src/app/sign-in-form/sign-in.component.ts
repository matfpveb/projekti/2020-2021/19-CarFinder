import { Component, OnInit } from '@angular/core';
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { JWTService }  from "../shared/jwt-service";
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  //url that we use as a constant in http requests
  private readonly urls = {
    putUser: "http://localhost:3000/api/user/signin"
  }

  //local variables that stores all necessary information for user sign in
  username: string;
  password: string;

  constructor(private http: HttpClient, private router: Router) {
    if (JWTService.getAccessToken() != null) {
      this.router.navigateByUrl("/home");
    }
  }

  //if we didnt set correct password
  public wrongPasswordAlert() {
    Swal.fire({
      //title: 'Warning',
      text: 'Wrong username or password!',
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: 'Try again!',
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true,
    })
  }

  //sign in request
  public signIn(): void {
    const body = { username: this.username, password: this.password };

    this.http
    .put<any>(this.urls.putUser, body).subscribe({
      next: data => {
        // if we set correct username and password we get access rights via access token
        JWTService.setAccessTokenPayload(data.accessToken, data.payload);
        this.router.navigateByUrl("/home")
      },
      error: error => {
        //if we make a mistake in password or username we inform user about that
        console.log(error);
        this.wrongPasswordAlert();
      }
    });
  }


  ngOnInit(): void {}
}
