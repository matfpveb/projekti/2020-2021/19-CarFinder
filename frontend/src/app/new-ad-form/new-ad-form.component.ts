import { HttpClient } from '@angular/common/http';
import { Component, NgModule, OnInit } from '@angular/core';
import { CustomSearchFormComponent } from '../custom-search-form/custom-search-form.component';
import { JWTService } from "../shared/jwt-service";
import { FileUploadModule } from 'primeng/fileupload';
import Swal from 'sweetalert2';

interface Car_Class {
  name: string,
  code: string
}

interface Car_Transmissions {
  name: string,
  code: string
}

interface Car_Drivetrain {
  name: string,
  code: string
}

interface Ad_Bargaining {
  name: string,
  code: string
}

interface Car_Manufacturers {
  name: string,
  code: string
}

interface Car_Models {
  name: string,
  code: string
}

interface Car_Engine {
  name: string,
  code: string
}

@Component({
  selector: 'app-new-ad-form',
  templateUrl: './new-ad-form.component.html',
  styleUrls: ['./new-ad-form.component.css']
})
export class NewAdFormComponent implements OnInit {

  //urls that we use as constants in our http requests
  private readonly urls = {
    postAd: "http://localhost:3000/api/ad",
    getManufacturers: "http://localhost:3000/api/car/manufacturers",
    getModels: "http://localhost:3000/api/car/"
  }

  //arrays of all possible values that we can use to create new ad
  car_class: Car_Class = { name: "", code: "" };
  car_classes: Car_Class[];

  car_transmission: Car_Transmissions = { name: "", code: "" };
  possible_transmissions: Car_Transmissions[];

  car_drivetrain: Car_Drivetrain = { name: "", code: "" };
  possible_drivetrain: Car_Drivetrain[];

  ad_bargaining: Ad_Bargaining = { name: "", code: "" };
  possible_bargaining: Ad_Bargaining[];

  car_manufacturer: Car_Manufacturers = { name: "", code: "" };
  possible_manufacturers: Car_Manufacturers[] = [];

  car_model: Car_Models = { name: "", code: "" };
  possible_models: Car_Models[] = [];

  car_engine: Car_Engine = { name: "", code: "" };
  possible_engines: Car_Engine[];


  constructor(private http: HttpClient, public customSearchForm:CustomSearchFormComponent) {
    //initialize all possible values for specific constant arrays
    this.car_classes = [
      { name: 'Cabriolet', code: 'CAB' },
      { name: 'SUV', code: 'SUV' },
      { name: 'Wagon', code: 'WAG' },
      { name: 'Sedan', code: 'SED' },
      { name: 'Hatchback', code: 'HAT' }
    ];

    this.possible_transmissions = [
      { name: "Continuously Variable Transmission (CVT)", code: 'CVT' },
      { name: "Manual Transmission (MT)", code: 'MT' },
      { name: "Automatic Transmission (AT)", code: 'AT' },
      { name: "Automated Manual Transmission (AM)", code: 'AM' },
    ];

    this.possible_drivetrain = [
      { name: 'RWD', code: 'rwd' },
      { name: 'FWD', code: 'fwd' },
      { name: '4WD', code: '4wd' },
      { name: 'AWD', code: 'awd' }
    ];

    this.possible_bargaining = [
      { name: 'Yes', code: 'yes' },
      { name: 'No', code: 'no' },
    ];


    this.possible_engines = [
      { name: 'Diesel', code: 'diesel' },
      { name: 'Petrol', code: 'petrol' }
    ];

  }

  //local variables that stores currently selected values of ad properties
  manufacturer: string = null;
  model: string = null;
  class: string = null;
  color: string = null;
  transmission: string = null;
  engine: string = null;
  model_year: number = null;
  car_year: number = null;
  displacement: number = null;
  price: number = null;
  timestamp: string = new Date().getTime().toString();
  fileToUpload: File = null;
  img: String = null;
  drivetrain: string = null;
  milleage: number = null;
  registration: string = null;
  description: string = null;

  //popUp handler
  public newAdInfo(text : string){
    Swal.fire({
      //title: 'Warning',
      text: text,
      icon: 'info',
      iconColor: "#521e63ff",
      confirmButtonText: 'OK!',
      confirmButtonColor: "#521e63ff",
      background: 'gray',
      allowEscapeKey: true,
      allowEnterKey: true,
      timer: 7000,
      timerProgressBar: true
    })
  }

  //http request for write in database
  submitAd(): void {
    //set access rights
    const headers = { "x-access-token": JWTService.getAccessToken() };
    let body = {
      "username": JWTService.getPayload().username,
      "car": {},
      "ad": {
        "timestamp": this.timestamp
      }
    };

    //load information into request body
    if (this.car_manufacturer.name.trim().length != 0) {
      body.car["manufacturer"] = this.car_manufacturer.name;
    }
    if (this.car_model.name.trim().length != 0) {
      body.car["model"] = this.car_model.name;
    }
    if (this.car_class.name.trim().length != 0) {
      body.car["class"] = this.car_class.name;
    }
    if (this.color != null && this.color.trim().length != 0) {
      body.car["color"] = this.color;
    }
    if (this.car_transmission.name.trim().length != 0) {
      body.car["transmission"] = this.car_transmission.name;
    }
    if (this.car_engine.name.trim().length != 0) {
      body.car["engine"] = this.car_engine.name;
    }
    if (this.model_year != null && this.model_year >= 1979) {
      body.car["model_year"] = this.model_year;
    }
    if (this.car_drivetrain.name.trim().length != 0) {
      body.car["drivetrain"] = this.car_drivetrain.name;
    }
    if (this.displacement != null && this.displacement > 0) {
      body.car["displacement"] = this.displacement;
    }

    //if user left some fields empty we inform it to fill all fields with values
    if(
      this.car_manufacturer.name.trim().length == 0 ||
      this.car_model.name.trim().length != 0 ||
      this.car_class.name.trim().length != 0 ||
      this.color.trim().length != 0 ||
      this.car_transmission.name.trim().length != 0 ||
      this.car_engine.name.trim().length != 0 ||
      this.model_year != null ||
      this.displacement != null ||
      this.car_drivetrain.name.trim().length != 0
    ){
      this.newAdInfo("You need to fill all fields about car!")
    }

    //check validity of given values
    if (this.car_year != null && this.car_year >= 1979) {
      body.ad["car_year"] = this.car_year;
    }
    if (this.price != null && this.price > 0) {
      body.ad["price"] = this.price;
    }
    if (this.registration != null && this.registration.trim().length != 0) {
      body.ad["registration"] = this.registration;
    }
    if (this.img != null) {
      body.ad["img"] = this.img;
    }
    if (this.description != null && this.description.trim().length != 0) {
      body.ad["description"] = this.description;
    }
    if (this.milleage != null && this.milleage > 0) {
      body.ad["milleage"] = this.milleage;
    }
    if (this.ad_bargaining.name.trim().length != 0) {
      body.ad["bargaining"] = this.ad_bargaining.name.toLowerCase();
    }

    //send http request
    this.http
      .post<any>(this.urls.postAd, body, { headers }).subscribe({
        next: data => {
          this.newAdInfo('New ad has been added!');
        },
        error: error => {
          console.log(error);
          this.newAdInfo('You need to fill all fields correctly!');
        }
      });
  }

  //function that adds file from our system
  uploadFile(filesList: FileList): void {
    const filesArray = Array.from(filesList);
    var reader = new FileReader();
    reader.readAsDataURL(filesArray[0]);
    reader.onload = () => {
      this.img = reader.result.toString();
    };
  }

  //explanation of these load functions can be found in custom-search-form.ts file
  //these function just loads data from database and stores it into local arrays
  load_manufacturers() {
    this.http.get<any>(this.urls.getManufacturers, {}).subscribe({
      next: data => {
        for (let i in data['manufacturers']) {
          this.possible_manufacturers.push({ name: data['manufacturers'][i], code: data['manufacturers'][i] });
        }

        this.possible_manufacturers = this.possible_manufacturers.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
      },
      error: error => {
        console.log(error);
      }
    })
  }

  load_models() {
    this.http.get<any>(this.urls.getModels + this.car_manufacturer.name, {}).subscribe({
      next: data => {
        this.possible_models.length = 0;

        for (let i in data['models']) {
          this.possible_models.push({ name: data['models'][i], code: data['models'][i] });
        }

        this.possible_models = this.possible_models.sort((a, b) =>
          (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
      },
      error: error => {
        console.log(error);
      }
    })
  }

  ngOnInit(): void {
    this.load_manufacturers();
  }

}
