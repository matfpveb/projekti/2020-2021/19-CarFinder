const adServices = require('../services/ad');

const getAdSmart = async(req, res, next) => {
    try {
        var ads = await adServices.getAdSmart(req.body);
        res.status(200).json(ads);
    } catch (err) {
        next(err);
    }
}

const getAdComplete = async(req, res, next) => {
    try {
        const ads = await adServices.getAdComplete(req.body);
        res.status(200).json(ads);
    } catch (err) {
        next(err);
    }
}

const getMyAds = async(req, res, next) => {
    try {
        const ads = await adServices.getMyAds(req.body);
        res.status(200).json(ads);
    } catch (err) {
        next(err);
    }
}

const deleteAd = async(req, res, next) => {
    try {
        const info = await adServices.deleteAd(req.body);

        if (info.deletedCount == 1) {
            res.status(200).json();
        } else {
            res.status(404).json();
        }
    } catch (err) {
        next(err);
    }
}

const putAd = async(req, res, next) => {
    try {
        const { srcParams, updParams } = req.body;
        const info = await adServices.putAd(srcParams, updParams);

        if (info.ok == 0) {
            res.status(404).json();
        } else {
            res.status(200).json();
        }
    } catch (err) {
        next(err);
    }
}

const postAd = async(req, res, next) => {
    try {
        const info = await adServices.postAd(req.body);

        if ("ok" in info) {
            res.status(409).json({ error: "Such car doesn't exist" });
        } else {
            res.status(200).json();
        }
    } catch (err) {
        next(err);
    }
}

module.exports = {
    getAdSmart,
    getAdComplete,
    getMyAds,
    deleteAd,
    putAd,
    postAd
}