import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAdsFormComponent } from './my-ads-form.component';

describe('MyAdsFormComponent', () => {
  let component: MyAdsFormComponent;
  let fixture: ComponentFixture<MyAdsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyAdsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAdsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
