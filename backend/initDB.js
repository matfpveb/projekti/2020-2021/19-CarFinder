const mongoose = require('mongoose');
const { Car, User, Ad } = require('./models/models');

const databaseString = "mongodb://localhost:27017/CarFinder";

mongoose.connect(databaseString, {
    useNewUrlParser: true,
    useUnifiedTopology: true  
});
 
mongoose.connection.once('open', () => {
     console.log('Connection successful!');
});

const dbCar = require('./database/json/car_joined.json');
const dbUser = require('./database/json/user_joined.json');
const dbAd = require('./database/json/ad_joined.json');

const carPromise = Car.insertMany(dbCar);
const userPromise = User.insertMany(dbUser);
const adPromise = Ad.insertMany(dbAd);
Promise.all([carPromise, userPromise, adPromise]).then(_ => {
    console.log("DB initialization finished successfully.");
    mongoose.disconnect();
});