const mongoose = require('mongoose');
const { Ad, User, Car } = require('../models/models');
const adSearch = require('./search');

const getAdSmart = async(params) => {
    return await adSearch(params, true);
}

const getAdComplete = async(params) => {
    return await adSearch(params, false);
}

const getMyAds = async(params) => {
    var ads = await Ad
        .find({})
        .populate("user", null, { "username": { $eq: params.username }})
        .populate("car", null, {})
        .exec();
    ads = ads.filter((ad, _) => ad.user != null);

    return ads;
}

const putAd = async(srcParams, updParams) => {
    Object.keys(srcParams).forEach(key => srcParams[key] === undefined && delete srcParams[key]);
    Object.keys(updParams).forEach(key => updParams[key] === undefined && delete updParams[key]);

    return Ad.updateOne(srcParams, { $set: updParams });
}

const deleteAd = async(params) => {
    const info = await Ad.deleteOne({_id: params._id }).exec();
    return info;
}

const postAd = async(params) => {
    carParams = params.car;
    adParams = params.ad;

    const user = await User.findOne({ username: params.username });
    if (!user) {
        return { "ok": 0 };
    }

    const car = await Car.findOne(carParams);
    if (!car) {
        return { "ok": 0 };
    }

    adParams["user"] = user._id;
    adParams["car"] = car._id;

    return Ad.insertMany([adParams]);
}

module.exports = {
    getAdSmart,
    getAdComplete,
    getMyAds,
    deleteAd,
    putAd,
    postAd
}