const mongoose = require('mongoose');

const carSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    manufacturer: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    model_year: {
        type: Number,
        required: true
    },
    transmission:{
        type: String,
        enum: [
            "Continuously Variable Transmission (CVT)",
            "Manual Transmission (MT)",
            "Automatic Transmission (AT)",
            "Automated Manual Transmission (AM)"
        ],
        required: true
    },
    engine: {
        type: String,
        enum: ["Diesel", "Petrol"],
        required: true
    },
    color: {
        type: String,
        required: true
    },
    class: {
        type: String,
        enum: ["Cabriolet", "SUV", "Wagon", "Sedan", "Hatchback"],
        required: true
    },
    displacement: {
        type: Number,
        required: true
    },
    drivetrain: {
        type: String,
        enum: ["RWD", "FWD", "4WD", "AWD"],
        required: true
    }
});

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true
    },
    email: {
        type: String, 
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: "basic",
        enum: ["basic", "admin"]
    },
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    city: String,
    age: {
        type: Number,
        required: true
    },
    accessToken: {
        type: String,
        required: true
    }
});

const adSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    car: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car"
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    timestamp: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: String,
    registration: {
        type: String,
        required: true,
    },
    milleage: {
        type: Number,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    bargaining: {
        type: String,
        default: "yes",
        enum: ["yes", "no"]
    },
    car_year: {
        type: Number,
        required: true
    }
});

module.exports = {
    Car: mongoose.model("Car", carSchema, "Car"),
    User: mongoose.model("User", userSchema, "User"),
    Ad: mongoose.model("Ad", adSchema, "Ad")
};