const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const fs = require('fs');

require('dotenv').config();

var idCars = new Map();
var dbCar = require('./database/json/car.json');
for (var i = 0; i < dbCar.length; i++) {
    dbCar[i]._id = new mongoose.Types.ObjectId();
    idCars[[dbCar[i].manufacturer, dbCar[i].model, dbCar[i].model_year]] = dbCar[i]._id;
}

var idUsers = new Map();
var dbUser = require('./database/json/user.json');
for (var i = 0; i < dbUser.length; i++) {
    dbUser[i]._id = new mongoose.Types.ObjectId();
    dbUser[i].password = bcrypt.hashSync(dbUser[i].password, 10);

    const accessToken = jwt.sign({
        userId: dbUser[i]._id },
        process.env.JWT_SECRET, {
        expiresIn: "30d"
    });

    dbUser[i]["accessToken"] = accessToken;
    idUsers[[dbUser[i].username]] = dbUser[i]._id;
}

var dbAd = require('./database/json/ad.json');
for (var i = 0; i < dbAd.length; i++) {
    dbAd[i]._id = new mongoose.Types.ObjectId();
    dbAd[i].car = idCars[[dbAd[i].manufacturer, dbAd[i].model, dbAd[i].model_year]];
    dbAd[i].user = idUsers[[dbAd[i].username]];

    var bitmap = fs.readFileSync("./img.png");
    const buffer = new Buffer(bitmap).toString("base64");
    dbAd[i].img = "data:image/png;base64," + buffer;

    delete dbAd[i].manufacturer;
    delete dbAd[i].model;
    delete dbAd[i].model_year;
    delete dbAd[i].username;
}

console.log("Creating db for Car...")
var jsonCar = JSON.stringify(dbCar);
var fsCar = require('fs');
fsCar.writeFile('./database/json/car_joined.json', jsonCar, 'utf8', (err) => {});
console.log("dbCar finished!")

console.log("Creating db for User...")
var jsonUser = JSON.stringify(dbUser);
var fsUser = require('fs');
fsUser.writeFile('./database/json/user_joined.json', jsonUser, 'utf8', (err) => {});
console.log("dbUser finished!")

console.log("Creating db for Ad...")
var jsonAd = JSON.stringify(dbAd);
var fsAd = require('fs');
fsAd.writeFile('./database/json/ad_joined.json', jsonAd, 'utf8', (err) => {});
console.log("dbAd finished!")