const express = require('express');
const adController = require('../../controllers/ad');
const auth = require('../../middleware/auth');

const router = express.Router();

// Fetching ads from db
router.get('/smart', auth.verifyToken, auth.grantAccess("readAny", "ad"), adController.getAdSmart);
router.post('/complete', auth.verifyToken, auth.grantAccess("readAny", "ad"), adController.getAdComplete);
router.post('/my', auth.verifyToken, auth.grantAccess("readOwn", "ad"), adController.getMyAds);

// Adding new ad to the db
router.post('/', auth.verifyToken, auth.grantAccess("createOwn", "ad"), adController.postAd);

// Deleting existing ad from the db
router.delete('/', auth.verifyToken, auth.grantAccess("deleteOwn", "ad"), adController.deleteAd);

module.exports = router;